/**
 * Function to extrapulate a date by a set amount of BiMonthly periods
 * @param startDate (Date) : The starting date to extrapulate upon
 * @param BiMonthJump (Number) : The amount of BiMonthly periods to extrapulate upon
 * @returns (Number) : the extrapulated date in milliseconds
 */
export const nextBiMonthlyDay = (startDate: Date, BiMonthJump: number) => {
    let daysInBiMonthlyPeriod = 0;

    if (startDate.getDate() <= 28) {
        for (let i = 0; i < BiMonthJump*2; i++) {
            daysInBiMonthlyPeriod += new Date(startDate.getFullYear(), startDate.getMonth()+1+i, 0).getDate();
        }
    } else {
        for (let i = 0; i < BiMonthJump*2; i++) {
            let diff = (new Date(startDate.getFullYear(), startDate.getMonth()+1+i, 0).getDate() - startDate.getDate());
            if (startDate.getDate() > new Date(startDate.getFullYear(), startDate.getMonth()+2+i, 0).getDate()) {
                daysInBiMonthlyPeriod += (diff + new Date(startDate.getFullYear(), startDate.getMonth()+2+i, 0).getDate()); 
            } else {
                daysInBiMonthlyPeriod += (diff + startDate.getDate());
            }
        }
    }

    let millisecondsPerMonthlyPeriod: number = daysInBiMonthlyPeriod * 24 * 60 * 60 * 1000;
    return Math.floor(startDate.getTime() + millisecondsPerMonthlyPeriod);
}

/**
 * Function to extrapulate a date by a set amount of months
 * @param startDate (Date) : The starting date to extrapulate upon
 * @param monthJump (Number) : The amount of Months to extrapulate upon
 * @returns (Number) : the extrapulated date in milliseconds
 */
export const nextMonthlyDay = (startDate: Date, monthJump: number) => {
    let daysInMonthlyPeriod: number = 0;
    
    if (startDate.getDate() <= 28) {
        for (let i = 0; i < monthJump; i++) {
            daysInMonthlyPeriod += new Date(startDate.getFullYear(), startDate.getMonth()+1+i, 0).getDate();
        }
    } else {
        for (let i = 0; i < monthJump; i++) {
            let diff = (new Date(startDate.getFullYear(), startDate.getMonth()+1+i, 0).getDate() - startDate.getDate());
            if (startDate.getDate() > new Date(startDate.getFullYear(), startDate.getMonth()+2+i, 0).getDate()) {
                daysInMonthlyPeriod += (diff + new Date(startDate.getFullYear(), startDate.getMonth()+2+i, 0).getDate()); 
            } else {
                daysInMonthlyPeriod += (diff + startDate.getDate());
            }
        }
    }

    let millisecondsPerMonthlyPeriod: number = daysInMonthlyPeriod * 24 * 60 * 60 * 1000;
    return Math.floor(startDate.getTime() + millisecondsPerMonthlyPeriod);
}

/**
 * Function to extrapulate a date by a set amount of BiWeekly periods
 * @param startDate (Date) : The starting date to extrapulate upon
 * @param biWeekJump (Number) : The amount of BiWeekly periods to extrapulate upon
 * @returns (Number) : the extrapulated date in milliseconds
 */
export const nextBiweeklyDay = (startDate: Date, biWeekJump: number) => {
    let millisecondsPerBiweeklyPeriod: number = (biWeekJump*14) * 24 * 60 * 60 * 1000;
    return Math.floor(startDate.getTime() + millisecondsPerBiweeklyPeriod);
}

/**
 * Function to extrapulate a date by a set amount of weeks
 * @param startDate (Date) : The starting date to extrapulate upon
 * @param weekJump (Number) : The amount of Weeks to extrapulate upon
 * @returns (Number) : the extrapulated date in milliseconds
 */
export const nextWeeklyDay = (startDate: Date, weekJump: number) => {
    let millisecondsPerWeeklyPeriod: number = (weekJump*7) * 24 * 60 * 60 * 1000;
    return Math.floor(startDate.getTime() + millisecondsPerWeeklyPeriod);
}