import MyCalander from "./components/MyCalander/MyCalander";
import EventForm from "./components/EventForm/EventForm";
import EventList from "./components/EventList/EventList";
import MyModal from "./components/MyModal/MyModal";
import { useEffect, useState } from 'react';
import { Bill, UpdateBill } from './Interfaces';
import './App.css';

require('react-big-calendar/lib/css/react-big-calendar.css');

// !TODO
// 

function App() {
  const [eventData, setEventData] = useState<Bill[]>([]);
  const [modalData, setModalData] = useState<Bill[]>([{ _id: '', name: '', date: '', type: '', __v: '' }]);

  /**
   * onClick handler for clicking on a bill
   * @param e (React.MouseEvent) : event to pull data from
   */
  const onClick = (e: React.MouseEvent) => {
    // @ts-ignore
    getSingleEventData(e.target.id);

    var modal: HTMLDivElement = document.getElementById('myModal') as HTMLDivElement;
    modal.style.display = "block";
  };

  /**
   * Function to Close the Modal
   */
  const modalClose = () => {
    var modal: HTMLDivElement = document.getElementById('myModal') as HTMLDivElement;
    modal.style.display = "none";
  };

  /**
   * Function to fetch a single event from the API by an id
   * @param id (String) : the id to search by
   */
  const getSingleEventData = async (id: String) => {
    fetch(`/api/v1/events/${id}`).then(
      response => response.json()
    ).then(
      data => {
        setModalData(data);
      }
    )
  };

  /**
   * Function to delete a single event from the API by an id
   */
  const deleteEventData = async () => {
    fetch(`/api/v1/events/${Object.values(modalData)[0]._id}`, {
      method: 'DELETE',
    }).then(() => {
      console.log('Event Deleted');
      modalClose();
    })
  };

  /**
   * function to update a single event from the API by an id 
   * @param event (UpdateBill) : object of data to be updated with
   */
  const updateEventData = async (event:UpdateBill) => {
    fetch(`/api/v1/events/${Object.values(modalData)[0]._id}`, {
      method: 'PATCH',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(event),
    }).then(() => {
      console.log('Event Updated');
      modalClose();
    })
  };

  useEffect(() => {
    fetch("/api/v1/events").then(
      response => response.json()
    ).then(
      data => {
        setEventData(data.events);
      }
    )
  }, []);

  return (
    <div className="App">

      <div className="left">
        <MyCalander eventData={eventData} />
      </div>
      <div className="right">
        <EventForm />
        <EventList bills={eventData} onClick={onClick}/>
      </div>

      <MyModal modalClose={modalClose} 
               deleteEvent={deleteEventData} 
               updateEvent={updateEventData}
               ModalData={modalData}
      />

    </div>
  );
}

export default App;