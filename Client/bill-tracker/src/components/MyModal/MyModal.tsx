import { ContainerDiv, StyledButton, StyledExit, StyledH2, StyledInput, StyledLabel, StyledOption, StyledSelect, Wrapper } from "./MyModal.styles";
import { useState, useEffect } from "react";
import { UpdateBill, Bill } from "../../Interfaces";

type Props = {
    modalClose: () => void;
    deleteEvent: () => void;
    updateEvent: (event:UpdateBill) => void;
    ModalData: Bill[];
}

const MyModal: React.FC<Props> = ({ modalClose, deleteEvent, updateEvent, ModalData }) => {
    const [Modalname, setModalName] = useState('');
    const [Modaldate, setModalDate] = useState('');
    const [Modaltype, setModalType] = useState('');

    useEffect(() => {
        setModalName(Object.values(ModalData)[0].name);
        setModalDate(Object.values(ModalData)[0].date);
        setModalType(Object.values(ModalData)[0].type);
    }, [ModalData]);

    const event:UpdateBill = {name: Modalname, date: Modaldate, type: Modaltype};
    
    return (
        <Wrapper>
            <div id="myModal" className="modal"> 
                <div className="modal-content">
                    <StyledExit className="close" onClick={modalClose}>&times;</StyledExit>
                    <StyledH2>Event Details Modal</StyledH2>
                        <div>

                            <ContainerDiv className="container">
                                <StyledInput 
                                    type='text' 
                                    value={Modalname}
                                    onChange={(e) => setModalName(e.target.value)}>
                                </StyledInput>
                                <StyledLabel>Bill Name</StyledLabel>
                            </ContainerDiv>

                            <ContainerDiv className="container">
                                <StyledInput 
                                    type='date' 
                                    value={Modaldate.split('T')[0]} 
                                    onChange={(e) => setModalDate(e.target.value)}>
                                </StyledInput>
                                <StyledLabel>Payment Date</StyledLabel>
                            </ContainerDiv>

                            <ContainerDiv className="container">
                                <StyledSelect 
                                    value={Modaltype} 
                                    onChange={(e) => setModalType(e.target.value)}
                                    >
                                    <StyledOption value="Weekly">Weekly</StyledOption>
                                    <StyledOption value="BiWeekly">BiWeekly</StyledOption>
                                    <StyledOption value="Monthly">Monthly</StyledOption>
                                    <StyledOption value="BiMonthly">BiMonthly</StyledOption>
                                </StyledSelect>
                                <StyledLabel>Payment Frequency</StyledLabel>
                            </ContainerDiv>

                        </div>
                    <StyledButton onClick={deleteEvent}>Delete</StyledButton>
                    <StyledButton onClick={() => updateEvent(event)}>Update</StyledButton>
                </div>
            </div>
        </Wrapper>
    )
};

export default MyModal;