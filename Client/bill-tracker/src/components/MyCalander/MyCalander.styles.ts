import styled from "styled-components";

export const Wrapper = styled.div`
    width: 100%;
    border-radius: 5px;
    box-shadow: 0px 20px 40px -20px var(--Blue);

    .rbc-off-range-bg {
        background-color: var(--LightBlue);
    }

    .rbc-today {
        background-color: var(--LightGrey);
    }

    .rbc-event {
        background-color: var(--Blue);
    }
`;