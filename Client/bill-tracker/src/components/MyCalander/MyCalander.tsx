import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import { Bill, CalanderData, UpdateBill } from '../../Interfaces';
import { useState, useEffect } from 'react';
import { nextWeeklyDay, nextBiweeklyDay, nextMonthlyDay, nextBiMonthlyDay } from '../../utils'
import { Wrapper } from './MyCalander.styles';

type Props = {
    eventData: Bill[];
}

const localizer = momentLocalizer(moment);

const MyCalander: React.FC<Props> = ({ eventData }) => {
    const [events, setEvents] = useState<CalanderData[]>([]);

    useEffect(() => {
        formatEventData();
    }, [eventData]);

    /**
     * Hard fix for AST time zone (GMT-0300)
     * @param event event of type BILL object
     * @returns Date adjusted for AST
     */
    const adjustDate = (event:Bill) => {
        let temp = new Date(event.date);
        return new Date(temp.setTime(temp.getTime() + 3 * 60 * 60 * 1000));
    }
    
    /**
     * Function for looping through events and extrapalating out a few ocurances of the event
     */
    const formatEventData = async () => {
        setEvents([]);
        eventData.forEach(event => {
            let temp: Date = adjustDate(event);
            switch (event.type) {
                case 'Weekly':
                    for (let i = 0; i < 5; i++) {
                        let newEvent: CalanderData = { allDay: true, end: new Date(), start: new Date(), title: '' };
                        let newDate: number = nextWeeklyDay(temp, i);
    
                        newEvent.end = new Date(newDate);
                        newEvent.start = new Date(newDate);
                        newEvent.title = event.name;
    
                        setEvents(events => [...events, newEvent]);
                        checkForUpdate(new Date(newDate), event);
                    }
                    break;
                case 'BiWeekly':
                    for (let i = 0; i < 3; i++) {
                        let newEvent: CalanderData = { allDay: true, end: new Date(), start: new Date(), title: '' };
                        let newDate: number = nextBiweeklyDay(temp, i);

                        newEvent.end = new Date(newDate);
                        newEvent.start = new Date(newDate);
                        newEvent.title = event.name;
    
                        setEvents(events => [...events, newEvent]);
                        checkForUpdate(new Date(newDate), event);
                    }
                    break;
                case 'Monthly':
                    for (let i = 0; i < 2; i++) {
                        let newEvent: CalanderData = { allDay: true, end: new Date(), start: new Date(), title: '' };
                        let newDate = new Date(nextMonthlyDay(temp, i));

                        newEvent.end = new Date(newDate);
                        newEvent.start = new Date(newDate);
                        newEvent.title = event.name;
        
                        setEvents(events => [...events, newEvent]);
                        checkForUpdate(new Date(newDate), event);
                    }
                    break;
                case 'BiMonthly':
                    for (let i = 0; i < 1; i++) {
                        let newEvent: CalanderData = { allDay: true, end: new Date(), start: new Date(), title: '' };
                        let newDate = new Date(nextBiMonthlyDay(temp, i));

                        newEvent.end = new Date(newDate);
                        newEvent.start = new Date(newDate);
                        newEvent.title = event.name;
        
                        setEvents(events => [...events, newEvent]);
                        checkForUpdate(new Date(newDate), event);
                    }
                    break;
            }
        });
    };

    /**
     * Function to Update the dates of events automaticaly
     * @param extrapDate (Date) : An extrapulated date
     * @param event (Bill) : The bill event
     */
    const checkForUpdate = (extrapDate: Date, event: Bill) => {
        let today = new Date(Date.now());
        if (today.getDate() === extrapDate.getDate() && 
            today.getMonth() === extrapDate.getMonth() && 
            today.getFullYear() === extrapDate.getFullYear()) {

            if (event.type === 'Monthly' || event.type === 'BiMonthly') {
                if (new Date(event.date).getDate() === extrapDate.getDate()) {
                    const UpdatedEvent: Bill = {_id: event._id, name: event.name, date: extrapDate.toString(), type: event.type, __v: event.__v};
                    updateEventData(UpdatedEvent);
                }
            } else {
                const UpdatedEvent: Bill = {_id: event._id, name: event.name, date: extrapDate.toString(), type: event.type, __v: event.__v};
                updateEventData(UpdatedEvent);
            }
        }
    }

    /**
     * Function to PATCH a bill event 
     * @param event (Bill) : The information to update with
     */
    const updateEventData = async (event:Bill) => {
        fetch(`/api/v1/events/${event._id}`, {
          method: 'PATCH',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(event),
        }).then(() => {
          console.log('Event Updated');
        })
      };

    return(
        <Wrapper>
            <Calendar 
            localizer={localizer}
            events={events}
            startAccessor="start"
            endAccessor="end"
            style={{ height: 800 }}
            />
        </Wrapper>
        );
}

export default MyCalander;