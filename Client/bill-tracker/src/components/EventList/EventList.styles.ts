import styled from "styled-components";

export const Wrapper = styled.div`
    width: 90%;
    margin: 0 auto; 
`;

export const StyledSpan = styled.span`
    cursor: pointer;
    position: relative;
    display: block;
    padding: .4em .4em .4em 2em;
    *padding: .4em;
    margin: .5em 0;
    background: var(--LightBlue);
    color: var(--Blue);
    text-decoration: none;
    -moz-border-radius: .3em;
    -webkit-border-radius: .3em;
    border-radius: 10em;
    transition: all .2s ease-in-out;

    &:hover {
        background: var(--Blue);
        color: var(--LightBlue);
        text-decoration:none;
        transform: scale(1.1);
    }
        
    &:before {
        content: counter(li);
        counter-increment: li;
        position: absolute;
        left: -1.3em;
        top: 50%;
        margin-top: -1.3em;
        background: var(--LightBlue);
        height: 2em;
        width: 2em;
        line-height: 2em;
        border: .3em solid #fff;
        text-align: center;
        font-weight: bold;
        -moz-border-radius: 2em;
        -webkit-border-radius: 2em;
        border-radius: 2em;
        color:var(--Blue);
    }

`;

export const StyledOL = styled.ol`
    counter-reset: li;
    list-style: none;
    *list-style: decimal;
    font-size: 15px;
    font-family: 'Raleway', sans-serif;
    padding: 0;
    margin-top: 0px;
    margin-bottom: 0px;

    ol {
        margin: 0 0 0 2em;
    }
`;

export const StyledHR = styled.hr`
    background-color: var(--Blue);
    height: 1px;
    width: 95%;
`;

export const BillContainer = styled.div`
    position: relative;
    left: -25px;
    width: 110%;
    height: 440px;
    padding-left: 25px;
    overflow-y: scroll;
    overflow-x: hidden; 

    /* width */
    ::-webkit-scrollbar {
        width: 10px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: var(--LightGrey);
        border-radius: 10px;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: var(--Blue);
        border-radius: 10px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: var(--Blue);
    }
`;