import { StyledSpan, StyledHR, StyledOL, Wrapper, BillContainer } from "./EventList.styles"
import { Bill } from '../../Interfaces';
import { useState, useEffect } from "react";

type Props = {
    bills: Bill[];
    onClick: (e: React.MouseEvent) => void;
}

const EventList: React.FC<Props> = ({ bills, onClick }) => {
    const [sortedEvents, setSortedEvents] = useState<Bill[]>([]);

    useEffect(() => {
        SortArray(bills);
    }, [bills]);

    /**
     * function to sort an array
     * @param bills Array of Type Bill 
     */
    const SortArray = (bills: Bill[]) => {
        bills.sort((a, b) => a.name.localeCompare(b.name))
        setSortedEvents(bills);
    };

    return(
        <Wrapper>
            <StyledHR/>
            <BillContainer>
                <StyledOL>
                {sortedEvents.map((bill, i) => (
                    <li key={i} onClick={(e) => {onClick(e)}}><StyledSpan id={bill._id}>{bill.name}</StyledSpan></li>
                ))}
                </StyledOL>
            </BillContainer>
        </Wrapper>
    )
}

export default EventList