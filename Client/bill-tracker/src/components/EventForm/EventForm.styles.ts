import styled from "styled-components";

export const Wrapper = styled.div`
    * {
        font-family: Helvetica , sans-serif;
        font-weight: light;
        -webkit-font-smoothing: antialiased;
    }
`;

export const StyledForm = styled.form`
    position: relative;
    display: inline-block;

    width: 100%;
    box-sizing: border-box;
    padding: 30px 25px;
    background-color: var(--LightBlue);
    border-radius: 40px;
    margin: 5px 0;
    left: 50%;
    transform: translate(-50%, 0);
`;

export const StyledLabel = styled.label`
    transition: all 0.25s cubic-bezier(.53,.01,.35,1.5);
    transform-origin: left center;
    color: var(--Blue);
    font-weight: 100;
    letter-spacing: 0.01em;
    font-size: 17px;
    box-sizing: border-box;
    padding: 10px 15px;
    display: block;
    position: absolute;
    margin-top: -40px;
    z-index: 2;
    pointer-events: none;
`;

export const StyledInput = styled.input`

    &[type=date]:required:invalid:: -webkit-datetime-edit {
        color: transparent;
    }

    &[type=date]:focus::-webkit-datetime-edit {
        color: var(--Blue) !important;
    }

    transition: all 0.25s cubic-bezier(.53,.01,.35,1.5);
    appearance: none;
    background-color: none;
    border: 1px solid var(--Blue);
    font-size: 17px;
    width: 100%;
    display: block;
    box-sizing: border-box;
    padding: 10px 15px;
    border-radius: 60px;
    color: var(--Blue);
    font-weight: 100;
    letter-spacing: 0.01em;
    position: relative;
    z-index: 1;

    &:focus {
        outline: none;
        background: var(--Blue);
        color: var(--LightBlue);
        margin-top: 30px;
    }

    &:valid {
        margin-top: 30px;
    }

    &:focus ~ label {
        transform: translate(0, -35px);
    }

    &:valid ~ label {
        text-transform: uppercase;
        font-style: italic;
        transform: translate(5px, -35px) scale(0.75);
    }
`;

export const StyledButton = styled.button`
    transition: all 0.25s cubic-bezier(.53,.01,.35,1.5);
    margin-top: 5px;
    background-color: white;
    border: 1px solid var(--Blue);
    line-height: 0;
    font-size: 17px;
    display: inline-block;
    box-sizing: border-box;
    padding: 20px 15px; 
    border-radius: 60px;
    color: var(--Blue);
    font-weight: 100;
    letter-spacing: 0.01em;
    position: relative;
    z-index: 1;
    
    &:hover, &:focus {
        color: var(--LightBlue);
        background-color: var(--Blue);
    }
`;

export const ContainerDiv = styled.div`
    position: relative;
    padding: 10px 0;

    container:first-of-type {
        padding-top: 0;
    }
    
    container:last-of-type {
        padding-bottom: 0;
    }
`;

export const StyledH2 = styled.h2`
    color: var(--Blue);
    font-weight: 100;
    letter-spacing: 0.01em;
    margin-bottom: 5px;
    margin-top: 5px;
    text-transform: uppercase;
    text-align: center;
`;

export const StyledSelect = styled.select`
    height: 44px;

    transition: all 0.25s cubic-bezier(.53,.01,.35,1.5);
    appearance: none;
    background-color: none;
    border: 1px solid var(--Blue);
    font-size: 17px;
    width: 100%;
    display: block;
    box-sizing: border-box;
    padding: 10px 15px;
    border-radius: 60px;
    color: var(--Blue);
    font-weight: 100;
    letter-spacing: 0.01em;
    position: relative;
    z-index: 1;

    &:focus {
        outline: none;
        background: var(--Blue);
        color: var(--LightBlue);
        margin-top: 30px;
    }

    &:valid {
        margin-top: 30px;
    }

    &:focus ~ label {
        transform: translate(0, -35px);
    }

    &:valid ~ label {
        text-transform: uppercase;
        font-style: italic;
        transform: translate(5px, -35px) scale(0.75);
    }
`;

export const StyledOption = styled.option`
    background-color: var(--LightBlue);
    color: var(--Blue);
`;
