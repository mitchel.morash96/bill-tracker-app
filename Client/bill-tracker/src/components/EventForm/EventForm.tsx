import { useState } from "react";
import { ContainerDiv, StyledButton, StyledForm, StyledH2, StyledInput, 
         StyledLabel, StyledOption, StyledSelect, 
         Wrapper } from "./EventForm.styles";

const EventForm = () => {
    const [name, setName] = useState('');
    const [date, setDate] = useState('');
    const [type, setType] = useState('Weekly');

    /**
     * Function to handle submiting the EventForm
     */
    const onSubmit = async () => {
        //e.preventDefault();
        const event = { name: name, date: date, type: type };

        fetch("/api/v1/events", {
          method: 'POST',
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(event),
        }).then(() => {
          console.log('New Event Added');
        })
    }

    return(
        <Wrapper>
            <StyledForm id="EventForm" onSubmit={onSubmit}>
                <StyledH2>Bill Input Form</StyledH2>
                <ContainerDiv className="container">
                    <StyledInput 
                        type="text"
                        required
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                    <StyledLabel htmlFor="name">Bill Name</StyledLabel>
                </ContainerDiv>

                <ContainerDiv className="container">
                    <StyledInput 
                        type="date"
                        required 
                        id="date"
                        value={date}
                        onChange={(e) => setDate(e.target.value)}
                    />
                    <StyledLabel htmlFor="date">Payment Date</StyledLabel>
                </ContainerDiv>

                <ContainerDiv className="container">
                    <StyledSelect 
                        id="payFreq" 
                        value={type}
                        onChange={(e) => setType(e.target.value)}
                    >
                        <StyledOption value="Weekly">Weekly</StyledOption>
                        <StyledOption value="BiWeekly">BiWeekly</StyledOption>
                        <StyledOption value="Monthly">Monthly</StyledOption>
                        <StyledOption value="Monthly">BiMonthly</StyledOption>
                    </StyledSelect>
                    <StyledLabel htmlFor="payFreq">Frequency Type</StyledLabel>
                </ContainerDiv>
                <StyledButton type="submit">Submit</StyledButton>
            </StyledForm>       
        </Wrapper>
    )
}

export default EventForm;