export interface Bill {
    _id: string,
    name: string,
    date: string,
    type: string,
    __v: string,
}

export interface UpdateBill {
    name: string,
    date: string,
    type: string,
}

export interface CalanderData {
    allDay: boolean,
    end: Date,
    start: Date,
    title: string,
}