// import "whatwg-fetch";
// import { rest } from 'msw';
// import { setupServer } from 'msw/node';

const request = require('supertest');
const app = require('./app');

// const server = setupServer(
//     rest.get('/api/v1/events', (req, res, ctx) => {
//         return res(ctx.status(200), ctx.json({ events: [{__v: 0, _id: '', name: '', date: '', type: ''}, 
//                                                         {__v: 0, _id: '', name: '', date: '', type: ''}]}));
//     })
// );

// beforeAll(() => server.listen());
// afterAll(() => server.close());
// afterEach(() => server.resetHandlers());

describe('Events API', () => {
    // it('GET /api/v1/events --> array of events', async () => {
    //     return request(app)
    //     .get('/api/v1/events')
    //     .expect('Content-Type', /json/)
    //     .expect(200)
    //     .then(response => {
    //         expect(response.body.events).toEqual(
    //             expect.arrayContaining([
    //                 expect.objectContaining({
    //                     __v: 1,
    //                     _id: expect.any(String),
    //                     name: expect.any(String),
    //                     date: expect.any(String),
    //                     type: expect.any(String),
    //                 }),
    //             ])
    //         );
    //     });
    // });
        
    // it('GET /api/v1/event --> Wrong route 404', () => {
    //     return request(app).get('api/v1/event').expect(404);
    // })

    // it('GET /api/v1/events/id --> specific event bt id', () => {
    //     return request(app)
    //     .get('/api/v1/events/62741c2b44108b5cbc79010a')
    //     .expect('Content-Type', /json/)
    //     .expect(200)
    //     .then(response => {
    //         expect(response.body.event).toEqual(
    //             expect.objectContaining({
    //                 __v: expect.any(Number),
    //                 _id: '62741c2b44108b5cbc79010a',
    //                 name: expect.any(String),
    //                 date: expect.any(String),
    //                 type: expect.any(String),
    //             }),
    //         );
    //     });
    // });

    // it('GET /api/v1/events/id --> 404 if not found', () => {
    //     return request(app).get('/api/v1/events/62741c2b44108b5cbc79010b').expect(404);
    // });

    // it('POST /api/v1/events --> create event', () => {
    //     return request(app)
    //     .post('/api/v1/events')
    //     .send({
    //         name: 'testing event',
    //         date: '2022-04-16T00:00:00.000Z',
    //         type: 'Weekly',
    //     })
    //     .expect('Content-Type', /json/)
    //     .expect(201)
    //     .then((response) => {
    //         expect(response.body.event).toEqual(
    //             expect.objectContaining({
    //                 name: 'testing event',
    //                 date: '2022-04-16T00:00:00.000Z',
    //                 type: 'Weekly',
    //             })
    //         );
    //     });
    // });

    // it('GET /api/v1/events --> validate request body', () => {
    //     return request(app).post('/api/v1/events').send({ name: 123}).expect(422);
    // });

    // it('PATCH /api/v1/events/id --> update event by id', () => {
    //     return request(app)
    //     .patch('/api/v1/events/62741c2b44108b5cbc79010a')
    //     .send({
    //         name: 'testing event Update',
    //         date: '2022-04-16T00:00:00.000Z',
    //         type: 'Weekly',
    //     })
    //     .expect('Content-Type', /json/)
    //     .expect(200)
    //     .then((response) => {
    //         expect(response.body.event).toEqual(
    //             expect.objectContaining({
    //                 __v: expect.any(Number),
    //                 _id: '62741c2b44108b5cbc79010a',
    //                 name: 'testing event Update',
    //                 date: '2022-04-16T00:00:00.000Z',
    //                 type: 'Weekly',
    //             })
    //         );
    //     });
    // });

    // it('PATCH /api/v1/events/id --> 404 if not found', () => {

    // })
   
    // it('DELETE /api/v1/events/id --> delete event by id', () => {
    //     return request(app)
    //     .delete('/api/v1/events/62741c2b44108b5cbc79010a')
    //     .expect('Content-Type', /json/)
    //     .expect(200)
    //     .then((response) => {
    //         expect(response.body.event).toEqual(
    //             expect.objectContaining({
    //                 __v: expect.any(Number),
    //                 _id: '62741c2b44108b5cbc79010a',
    //                 name: expect.any(String),
    //                 date: expect.any(String),
    //                 type: expect.any(String),
    //             })
    //         );
    //     });
    // });
            
    // it('DELETE /api/v1/events/id --> 404 if not found', () => {

    // })
});