const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Must Provide A Name'],
        trim: true,
        maxlength: [20, "Name Cant Be More Than 20 Characters"],
    },
    date: {
        type: Date,
        required: [true, 'Must Provide A Date'],
    },
    type: {
        type: String,
        required: [true, "Must Provide A Type For The Event"],
        trim: true,
    }
});

module.exports = mongoose.model('Event', EventSchema);